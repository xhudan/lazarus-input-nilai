unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, odbcconn, SQLDB, DB, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBCtrls, Interfaces;

type

  { TFormInputNilai }

  TFormInputNilai = class(TForm)
    DataSource1: TDataSource;
    NPM: TDBEdit;
    Tugas1: TDBEdit; 
    Tugas2: TDBEdit;
    Tugas3: TDBEdit;
    VClass1: TDBEdit;
    VClass2: TDBEdit;
    VClass3: TDBEdit;
    UTS: TDBEdit;
    UAS: TDBEdit; 
    Rata: TDBEdit;
    Grade: TEdit;
    LabelRata: TLabel;
    LabelGrade: TLabel;
    LabelNPM: TLabel;
    LabelTugas1: TLabel;
    LabelTugas2: TLabel;
    LabelTugas3: TLabel;
    LabelUTS: TLabel;
    LabelUAS: TLabel;
    LabelVClass1: TLabel;
    LabelVClass2: TLabel;
    LabelVClass3: TLabel; 
    Save: TButton;
    Clear: TButton;     
    ODBCConnection1: TODBCConnection;
    SQLQuery1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure ClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure Tugas1Change(Sender: TObject);
    procedure Tugas2Change(Sender: TObject);
    procedure Tugas3Change(Sender: TObject);
    procedure UASChange(Sender: TObject);
    procedure UTSChange(Sender: TObject);
    procedure VClass1Change(Sender: TObject);
    procedure VClass2Change(Sender: TObject);
    procedure VClass3Change(Sender: TObject);
  private

  public

  end;

var
  FormInputNilai: TFormInputNilai;

implementation

{$R *.lfm}

{ TFormInputNilai }

procedure TFormInputNilai.FormCreate(Sender: TObject);
//Tutorial
//https://www.youtube.com/watch?v=ipFlwZRIIMY
//https://www.youtube.com/watch?v=8evLn7Wb1-k
//https://wiki.freepascal.org/MS_Access
begin

end;

procedure TFormInputNilai.SaveClick(Sender: TObject);
var
  vtugas1, vtugas2, vtugas3, vvclass1, vvclass2, vvclass3, vuts, vuas, rataanUTS, rataanTotal: real;
  vnpm:string;
begin
try
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  vnpm:=NPM.Text;
  rataanUTS := (((vtugas1 + vtugas2 + vtugas3) / 3 + (vvclass1 + vvclass2 + vvclass3) / 3) / 2) * 0.4 + vuts * 0.6;
  rataanTotal := rataanUTS * 0.7 + vuas * 0.3;

  // Pastikan SQLQuery1 sedang tidak aktif
  if SQLQuery1.Active then
    SQLQuery1.Close;

  SQLQuery1.SQL.Text := 'INSERT INTO Nilai(NPM, Tugas1, Tugas2, Tugas3, VClass1, VClass2, VClass3, UTS, UAS, Rataan) VALUES(:NPM, :Tugas1, :Tugas2, :Tugas3, :VClass1, :VClass2, :VClass3, :UTS, :UAS, :Rataan)';

  // Set nilai parameter
  SQLQuery1.Params.ParamByName('NPM').Value := vnpm;
  SQLQuery1.Params.ParamByName('Tugas1').Value := vtugas1;
  SQLQuery1.Params.ParamByName('Tugas2').Value := vtugas2;
  SQLQuery1.Params.ParamByName('Tugas3').Value := vtugas3;
  SQLQuery1.Params.ParamByName('VClass1').Value := vvclass1;
  SQLQuery1.Params.ParamByName('VClass2').Value := vvclass2;
  SQLQuery1.Params.ParamByName('VClass3').Value := vvclass3;
  SQLQuery1.Params.ParamByName('UTS').Value := vuts;
  SQLQuery1.Params.ParamByName('UAS').Value := vuas;
  SQLQuery1.Params.ParamByName('Rataan').Value := rataanTotal;

  // Eksekusi query
  SQLQuery1.ExecSQL;

  // Commit transaksi
  SQLTransaction1.Commit;

  ShowMessage('Data berhasil disimpan!');

  //Re-Load
  SQLQuery1.SQL.Text := 'select * from Nilai';
  SQLQuery1.Open;

  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message);
    end;
  end;
end;


procedure TFormInputNilai.ClearClick(Sender: TObject);
begin
  NPM.clear;
  Tugas1.clear;
  Tugas2.clear;
  Tugas3.clear;
  VClass1.clear;
  VClass2.clear;
  VClass3.clear;
  UTS.clear;
  UAS.clear;
end;

procedure TFormInputNilai.Tugas1Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.Tugas2Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.Tugas3Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.UASChange(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.UTSChange(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.VClass1Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.VClass2Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

procedure TFormInputNilai.VClass3Change(Sender: TObject);
var
  vtugas1,vtugas2,vtugas3,vvclass1,vvclass2,vvclass3,vuts,vuas,rataanUTS,rataanTotal:real;
begin
  if (not TryStrToFloat(Tugas1.Text, vtugas1)) then
   begin
     vtugas1:=0;
   end
  else
   begin
     vtugas1:=StrToFloat(Tugas1.Text);
   end;

  if (not TryStrToFloat(Tugas2.Text, vtugas2)) then
   begin
     vtugas2:=0;
   end
  else
   begin
     vtugas2:=StrToFloat(Tugas2.Text);
   end;

  if (not TryStrToFloat(Tugas3.Text, vtugas3)) then
   begin
     vtugas3:=0;
   end
  else
   begin
     vtugas3:=StrToFloat(Tugas3.Text);
   end;

  if (not TryStrToFloat(VClass1.Text, vvclass1)) then
   begin
     vvclass1:=0;
   end
  else
   begin
     vvclass1:=StrToFloat(VClass1.Text);
   end;

  if (not TryStrToFloat(VClass2.Text, vvclass2)) then
   begin
     vvclass2:=0;
   end
  else
   begin
     vvclass2:=StrToFloat(VClass2.Text);
   end;

  if (not TryStrToFloat(VClass3.Text, vvclass3)) then
   begin
     vvclass3:=0;
   end
  else
   begin
     vvclass3:=StrToFloat(VClass3.Text);
   end;

  if (not TryStrToFloat(UTS.Text, vuts)) then
   begin
     vuts:=0;
   end
  else
   begin
     vuts:=StrToFloat(UTS.Text);
   end;

  if (not TryStrToFloat(UAS.Text, vuas)) then
   begin
     vuas:=0;
   end
  else
   begin
     vuas:=StrToFloat(UAS.Text);
   end;

  rataanUTS:=(((vtugas1+vtugas2+vtugas3)/3 + (vvclass1+vvclass2+vvclass3)/3)/2)*0.4 + vuts*0.6;
  rataanTotal:=rataanUTS*0.7 + vuas*0.3;
  Rata.Text:=FloatToStr(rataanTotal);
  if (rataanTotal>=85) then
   begin
     Grade.Text:='A';
   end
  else if (rataanTotal>=75) then
   begin
     Grade.Text:='B';
   end
  else if (rataanTotal>=65) then
   begin
     Grade.Text:='C';
   end
  else if (rataanTotal>=51) then
   begin
     Grade.Text:='D';
   end
  else
    begin
     Grade.Text:='E';
   end;
end;

end.

